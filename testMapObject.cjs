const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 
let cb = x => x;

const mapObject = require('../mapObject.cjs');

const result1 = mapObject(testObject, cb);
console.log(result1);

const result2 = mapObject(testObject);
console.log(result2);

const result3 = mapObject();
console.log(result3);

const result4 = mapObject([]);
console.log(result4);