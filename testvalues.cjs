const  values = require("../values.cjs");

const result1 = values({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
console.log(result1);

const result2 = values({});
console.log(result2);

const result3 = values([]);
console.log(result3);

const result4 = values();
console.log(result4);