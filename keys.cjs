var _ = require('underscore');

const keys = function keys(obj = {}) {
    return _.keys(obj);
}

module.exports = keys;