var _ = require('underscore');

const pairs = function pairs(obj = {}) {
    return _.pairs(obj);
}

module.exports = pairs;