const keys = require("../keys.cjs");

const result1 = keys({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
console.log(result1);

const result2 = keys({});
console.log(result2);

const result3 = keys([]);
console.log(result3);

const result4 = keys();
console.log(result4);