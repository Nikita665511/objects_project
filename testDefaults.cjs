const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 
const defaultProps = { name: 'Nikitha', age: 21, country: 'India'}; //default props

const defaults = require('../defaults.cjs');

const result1 = defaults(testObject, defaultProps);
console.log(result1);

const result2 = defaults(testObject, []);
console.log(result2);

const result3 = defaults([]);
console.log(result3);