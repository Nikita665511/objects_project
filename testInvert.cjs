const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const invert = require('../invert.cjs');

const result1 = invert(testObject);
console.log(result1);

const result2 = invert();
console.log(result2);

const result3 = invert([]);
console.log(result3);