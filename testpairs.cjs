const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const pairs = require('../pairs.cjs');

const result1 = pairs(testObject);
console.log(result1);

const result2 = pairs();
console.log(result2);

const result3 = pairs([]);
console.log(result3);