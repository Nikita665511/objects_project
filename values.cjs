var _ = require('underscore');

const values = function keys(obj = {}) {
    return _.values(obj);
}

module.exports = values;