var _ = require('underscore');

const invert = function invert(obj = {}) {
    return _.invert(obj);
}

module.exports = invert;