var _ = require('underscore');

const mapObject = function mapObject(obj = {}, cb = x => x) {
    const newMapObject = {}
    if (cb instanceof Function && obj instanceof Object) {
        for(let key in obj) {
            newMapObject[key] = cb(obj[key]);
        }
    }
    return newMapObject;
}

module.exports = mapObject;