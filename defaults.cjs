var _ = require('underscore');

const defaults = function defaults(obj = {}, defaultProps = {}) {
    return _.defaults(obj, defaultProps);
}

module.exports = defaults;